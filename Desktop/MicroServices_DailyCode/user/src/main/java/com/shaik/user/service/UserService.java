package com.shaik.user.service;

import com.shaik.user.entity.User;
import com.shaik.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public User createUser(User user) {
        log.info("Creating the User in Service Method");
        return userRepository.save(user);
    }
}
