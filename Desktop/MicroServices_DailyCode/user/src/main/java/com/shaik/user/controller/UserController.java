package com.shaik.user.controller;

import com.shaik.user.entity.User;
import com.shaik.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {


    @Autowired
    UserService userService;


    @PostMapping
    public User createUser(@RequestBody User user) {
        log.info("Creating the user of information :" + user);
        log.info("Creating the User In the User Controller");
        return userService.createUser(user);
    }


}
